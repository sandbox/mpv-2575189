# Commerce Shipment Message

Simple module defining a [message][0] type and a function to log shipping
information to [Commerce][1] orders.

It was developed for [Commerce Etrans][2] and [Commerce Urbano][3], two shipping
services for Argentina.

Both provide a service where they pick up the package and then deliver it to
the customer, so this module provides a way to log this information (pickup
requested and optionally estimated pickup and delivery dates) to the order.

This is an API module, only install it if some other module asks you to.



[0]: https://www.drupal.org/project/message           "Message"
[1]: https://www.drupal.org/project/commerce          "Commerce"
[2]: https://www.drupal.org/project/commerce_etrans   "Commerce Etrans"
[3]: https://www.drupal.org/project/commerce_urbano   "Commerce Urbano"

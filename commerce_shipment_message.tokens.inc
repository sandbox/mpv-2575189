<?php

/**
 * @file
 * Token integration for the Commerce shipment message module.
 */

/**
 * Implements hook_token_info().
 */
function commerce_shipment_message_token_info() {
  $info['tokens']['message']['shipping-service-name'] = array(
    'name' => t('Shipping service'),
    'description' => t('The name of the shipping service.'),
  );

  $info['tokens']['message']['shipping-pickup-date'] = array(
    'name' => t('Pickup date'),
    'description' => t('The package will be picked up.'),
  );

  $info['tokens']['message']['shipping-delivery-date'] = array(
    'name' => t('Delivery date'),
    'description' => t('The estimated date of delivery.'),
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function commerce_shipment_message_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'message' && !empty($data['message'])) {
    $message_wrapper = entity_metadata_wrapper('message', $data['message']);

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'shipping-service':
          $machine_name = $message_wrapper->message_shipping_service_name->value();
          $replacements[$original] = commerce_shipping_service_get_title($machine_name, 'display_title');
          break;

        case 'shipping-pickup-date':
          $timestamp = $message_wrapper->message_shipping_pickup_date->value();
          if (!empty($timestamp)) {
            $replacements[$original] = format_date($timestamp, 'commerce_shipping_message_long_date');
          }
          else {
            $replacements[$original] = t('Unreported');
          }
          break;

        case 'shipping-delivery-date':
          $timestamp = $message_wrapper->message_shipping_delivery_date->value();
          if (!empty($timestamp)) {
            $replacements[$original] = format_date($timestamp, 'commerce_shipping_message_long_date');
          }
          else {
            $replacements[$original] = t('Unreported');
          }
          break;

      }
    }
  }

  return $replacements;
}
